<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::resource('alquileres', 'AlquileresController');
Route::resource('areascomunes', 'AreasComunesController');
Route::resource('areascomunesreserva', 'AreasComunesReservaController');
Route::resource('cercanoscomments', 'CercanosCommentsController');
Route::resource('cercanos', 'CercanosController');
Route::resource('cercanoslugares', 'CercanosLugaresController');
Route::resource('cercanoslugaresusuarios', 'CercanosLugaresUsuariosController');
Route::resource('cobros', 'CobrosController');
Route::resource('cobrosusuario', 'CobrosUsuarioController');
Route::resource('condominios', 'CondominiosController');
Route::resource('emergencias', 'EmergenciasController');
Route::resource('live', 'LiveController');
Route::resource('noticias', 'NoticiasController');
Route::resource('notificaciones', 'NotificacionesController');
Route::resource('objetosperdidos', 'ObjetosPerdidosController');
Route::resource('pagos', 'PagosController');
Route::resource('propietarios', 'PropietariosController');
Route::resource('proyectopropuestascomments', 'ProyectoPropuestasCommentsController');
Route::resource('proyectopropuestas', 'ProyectoPropuestasController');
Route::resource('proyectoscomments', 'ProyectosCommentsController');
Route::resource('proyectos', 'ProyectosController');
Route::resource('publicidad', 'PublicidadController');
Route::resource('tiporespuesta', 'TipoRespuestaController');
Route::resource('tipoventas', 'TipoVentasController');
Route::resource('tipousuario', 'TipoUsuarioController');
Route::resource('tipopuesto', 'TipoPuestoController');
Route::resource('users', 'UsersController');
Route::resource('uservisita', 'UserVisitaController');
Route::resource('uservisitascontrol', 'UserVisitasControlController');
Route::resource('ventas', 'VentasController');
Route::resource('viviendas', 'ViviendaController');
Route::resource('viviendausuarios', 'ViviendaUsuariosController');
Route::resource('votacion', 'VotacionController');
Route::resource('votacionusuarios', 'VotacionUsuariosController');
Route::resource('votospropuesta', 'VotosPropuestaController');

Route::get('users/{idUsuario}/viviendas/{idCondominio}', 'ViviendaUsuariosController@getThisByCondominio');
Route::get('users/{id}/alquileres', 'AlquileresController@getThisByUser');
Route::get('users/{id}/alquileres/{state}', 'AlquileresController@getThisByFilter');
Route::get('users/{id}/areascomunes', 'AreasComunesController@getThisByUser');
Route::get('users/{id}/areascomunes/{state}', 'AreasComunesController@getThisByFilter');
Route::get('users/{id}/areascomunesreserva', 'AreasComunesReservaController@getThisByUser');
Route::get('users/{id}/areascomunesreserva/{state}', 'AreasComunesReservaController@getThisByFilter');
Route::get('users/{id}/cercanos', 'CercanosController@getThisByUser');
Route::get('users/{id}/cercanos/{state}', 'CercanosController@getThisByFilter');
Route::get('users/{id}/cercanoslugares', 'CercanosLugaresController@getThisByUser');
Route::get('users/{id}/cercanoslugares/{state}', 'CercanosLugaresController@getThisByFilter');
Route::get('users/{id}/cercanoslugaresusuarios', 'CercanosLugaresUsuariosController@getThisByUser');
Route::get('users/{id}/cercanoslugaresusuarios/{state}', 'CercanosLugaresUsuariosController@getThisByFilter');
Route::get('users/{id}/cobros', 'CobrosController@getThisByUser');
Route::get('users/{id}/cobros/{state}', 'CobrosController@getThisByFilter');
Route::get('users/{id}/cobrosusuario', 'CobrosUsuarioController@getThisByUser');
Route::get('users/{id}/cobrosusuario/{state}', 'CobrosUsuarioController@getThisByFilter');
Route::get('users/{id}/emergencias', 'EmergenciasController@getThisByUser');
Route::get('users/{id}/emergencias/{state}', 'EmergenciasController@getThisByFilter');
Route::get('users/{id}/live', 'LiveController@getThisByUser');
Route::get('users/{id}/live/{state}', 'LiveController@getThisByFilter');
Route::get('users/{id}/noticias', 'NoticiasController@getThisByUser');
Route::get('users/{id}/noticias/{state}', 'NoticiasController@getThisByFilter');
Route::get('users/{id}/notificaciones', 'NotificacionesController@getThisByUser');
Route::get('users/{id}/notificaciones/{state}', 'NotificacionesController@getThisByFilter');
Route::get('users/{id}/objetosperdidos', 'ObjetosPerdidosController@getThisByUser');
Route::get('users/{id}/objetosperdidos/{state}', 'ObjetosPerdidosController@getThisByFilter');
Route::get('users/{id}/pagos', 'PagosController@getThisByUser');
Route::get('users/{id}/pagos/{state}', 'PagosController@getThisByFilter');
Route::get('users/{id}/proyectopropuestas', 'ProyectoPropuestasController@getThisByUser');
Route::get('users/{id}/proyectopropuestas/{state}', 'ProyectoPropuestasController@getThisByFilter');
Route::get('users/{id}/proyectos', 'ProyectosController@getThisByUser');
Route::get('users/{id}/proyectos/{state}', 'ProyectosController@getThisByFilter');
Route::get('users/{id}/publicidad', 'PublicidadController@getThisByUser');
Route::get('users/{id}/publicidad/{state}', 'PublicidadController@getThisByFilter');
Route::get('users/{id}/tiporespuesta', 'TipoRespuestaController@getThisByUser');
Route::get('users/{id}/tiporespuesta/{state}', 'TipoRespuestaController@getThisByFilter');
Route::get('users/{id}/tipoventas', 'TipoVentasController@getThisByUser');
Route::get('users/{id}/tipoventas/{state}', 'TipoVentasController@getThisByFilter');
Route::get('users/{id}/users', 'UsersController@getThisByUser');
Route::get('users/{id}/users/{state}', 'UsersController@getThisByFilter');
Route::get('users/{id}/uservisita', 'UserVisitaController@getThisByUser');
Route::get('users/{id}/uservisita/{state}', 'UserVisitaController@getThisByFilter');
Route::get('users/{id}/uservisitascontrol', 'UserVisitasControlController@getThisByUser');
Route::get('users/{id}/uservisitascontrol/{state}', 'UserVisitasControlController@getThisByFilter');
Route::get('users/{id}/ventas', 'VentasController@getThisByUser');
Route::get('users/{id}/ventas/{state}', 'VentasController@getThisByFilter');
Route::get('users/{id}/viviendas', 'ViviendaController@getThisByUser');
Route::get('users/{id}/viviendas/{state}', 'ViviendaController@getThisByFilter');
Route::get('users/{id}/viviendausuarios', 'ViviendaUsuariosController@getThisByUser');
Route::get('users/{id}/viviendausuarios/{state}', 'ViviendaUsuariosController@getThisByFilter');
Route::get('users/{id}/votacion', 'VotacionController@getThisByUser');
Route::get('users/{id}/votacion/{state}', 'VotacionController@getThisByFilter');
Route::get('users/{id}/votacionusuarios', 'VotacionUsuariosController@getThisByUser');
Route::get('users/{id}/votacionusuarios/{state}', 'VotacionUsuariosController@getThisByFilter');
Route::get('users/{id}/votospropuesta', 'VotosPropuestaController@getThisByUser');
Route::get('users/{id}/votospropuesta/{state}', 'VotosPropuestaController@getThisByFilter');

Route::get('morosos', 'UsersController@getMorosos');
Route::get('directivos', 'UsersController@getDirectivos');
Route::get('cercanos/{id}/lugar', 'CercanosLugaresController@getThisByCercano');

Route::get('cercanos/{id}/comments', 'CercanosLugaresController@getCommentsByThis');
Route::get('proyectos/{id}/comments', 'ProyectosCommentsController@getCommentsByThis');
Route::get('proyectopropuestas/{id}/comments', 'ProyectoPropuestasCommentsController@getCommentsByThis');

Route::post('users/password/reset', 'UsersController@recoveryPassword');
Route::post('users/{id}/changepassword', "UsersController@changePassword");
Route::post('login', 'AuthenticateController@login');

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');