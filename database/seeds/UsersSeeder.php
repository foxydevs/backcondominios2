<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username'          =>  "admin",
            'password'          => bcrypt('foxylabs'),
            'email'             => "info@foxylabs.gt",
            'nombre'            => "Admin",
            'apellido'          => "",
            'dpi'               => "000000000",
            'descripcion'       => "Administrador de condominio",
            'nacimiento'        => "1995-01-06",
            'telefono'          => "+50254646431",
            'picture'           => "https://image.flaticon.com/icons/svg/17/17797.svg",
            'last_conection'    => date('Y-m-d H:m:s'),
            'youtube_channel'   => "",
            'casa'              => 1,
            'puesto'            => 1,
            'state'             => 1,
            'tipo'              => 1,
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);
    }
}
