<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjetosPerdidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objetos_perdidos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion')->nullable()->default(null);
            $table->string('titulo')->nullable()->default(null);

            $table->date('fecha')->nullable()->default(null);
            $table->string('comentarios')->nullable()->default(null);
            $table->string('foto')->nullable()->default(null);
            $table->double('recompensa')->nullable()->default(null);

            $table->integer('vivienda')->nullable()->default(null)->unsigned();
            $table->foreign('vivienda')->references('id')->on('viviendas')->onDelete('cascade');
            $table->integer('user')->nullable()->default(null)->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');

            $table->integer('condominio')->nullable()->default(null)->unsigned();
            $table->foreign('condominio')->references('id')->on('condominios')->onDelete('cascade');
            
            $table->integer('state')->nullable()->default(1);
            $table->integer('tipo')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objetos_perdidos');
    }
}
