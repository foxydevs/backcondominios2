<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCondominiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('condominios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable()->default(null);
            $table->string('ubicacion')->nullable()->default(null);
            $table->string('picture')->nullable()->default(null);
            $table->integer('viviendas')->nullable()->default(0);

            $table->string('direccion')->nullable()->default(null);
            $table->string('comentarios')->nullable()->default(null);
            $table->double('longitud',15,8)->nullable()->default(null);
            $table->double('latitud',15,8)->nullable()->default(null);

            $table->integer('state')->nullable()->default(1);
            $table->integer('tipo')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('condominios');
    }
}
