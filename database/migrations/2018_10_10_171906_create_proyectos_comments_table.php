<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyectosCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyectos_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mensaje')->nullable()->default(null);
            $table->string('asunto')->nullable()->default(null);

            $table->date('fecha')->nullable()->default(null);
            $table->timestamp('aut_fecha')->useCurrent();
            $table->string('comentario')->nullable()->default(null);

            $table->integer('user')->nullable()->default(null)->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');

            $table->integer('proyecto')->nullable()->default(null)->unsigned();
            $table->foreign('proyecto')->references('id')->on('proyectos')->onDelete('cascade');
            
            $table->integer('state')->nullable()->default(1);
            $table->integer('tipo')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyectos_comments');
    }
}
