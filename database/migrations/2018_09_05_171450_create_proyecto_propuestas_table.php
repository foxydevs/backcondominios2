<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyectoPropuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyecto_propuestas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion')->nullable()->default(null);
            $table->string('titulo')->nullable()->default(null);

            $table->date('fecha')->nullable()->default(null);
            $table->string('comentarios')->nullable()->default(null);
            $table->string('foto')->nullable()->default(null);
            $table->double('cantidad')->nullable()->default(null);
            $table->double('costo')->nullable()->default(null);

            $table->integer('proyecto')->nullable()->default(null)->unsigned();
            $table->foreign('proyecto')->references('id')->on('proyectos')->onDelete('cascade');

            $table->integer('user')->nullable()->default(null)->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            
            $table->integer('state')->nullable()->default(1);
            $table->integer('tipo')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyecto_propuestas');
    }
}
