<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('password');
            $table->string('email');
            $table->string('nombre')->nullable()->default(null);
            $table->string('apellido')->nullable()->default(null);
            $table->string('dpi')->nullable()->default(null);
            $table->string('descripcion')->nullable()->default(null);
            $table->date('nacimiento')->nullable()->default(null);
            $table->string('telefono')->nullable()->default(null);
            $table->string('youtube_channel')->nullable()->default(null);
            $table->string('picture')->default('http://foxylabs.xyz/Documentos/imgs/logo.png');
            $table->dateTime('last_conection');
            $table->integer('casa')->nullable()->default(null);
            $table->integer('puesto')->nullable()->default(1);
            $table->integer('directivo')->nullable()->default(0);

            $table->integer('state')->nullable()->default(1);
            $table->integer('tipo')->nullable()->default(1);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
