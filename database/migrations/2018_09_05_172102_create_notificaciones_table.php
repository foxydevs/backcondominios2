<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notificaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mensaje')->nullable()->default(null);
            $table->string('titulo')->nullable()->default(null);

            $table->date('fecha')->nullable()->default(null);
            $table->timestamp('aut_fecha')->useCurrent();
            $table->string('comentarios')->nullable()->default(null);
            $table->double('cantidad')->nullable()->default(null);

            $table->integer('sender')->nullable()->default(null)->unsigned();
            $table->foreign('sender')->references('id')->on('viviendas')->onDelete('cascade');

            $table->integer('receipt')->nullable()->default(null)->unsigned();
            $table->foreign('receipt')->references('id')->on('viviendas')->onDelete('cascade');
            
            $table->integer('state')->nullable()->default(1);
            $table->integer('tipo')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notificaciones');
    }
}
