<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropietariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propietarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion')->nullable()->default(null);

            $table->string('titulo')->nullable()->default(null);

            $table->integer('condominio')->nullable()->default(null)->unsigned();
            $table->foreign('condominio')->references('id')->on('condominios')->onDelete('cascade');

            $table->integer('tipo_usuario')->nullable()->default(null)->unsigned();
            $table->foreign('tipo_usuario')->references('id')->on('tipo_usuario')->onDelete('cascade');

            $table->integer('tipo_puesto')->nullable()->default(null)->unsigned();
            $table->foreign('tipo_puesto')->references('id')->on('tipo_puesto')->onDelete('cascade');

            $table->integer('user')->nullable()->default(null)->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            
            $table->integer('state')->nullable()->default(1);
            $table->integer('tipo')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propietarios');
    }
}
