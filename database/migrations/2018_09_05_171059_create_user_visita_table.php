<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserVisitaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_visita', function (Blueprint $table) {
            $table->increments('id');

            $table->string('comentarios')->nullable()->default(null);

            $table->integer('vivienda')->nullable()->default(null)->unsigned();
            $table->foreign('vivienda')->references('id')->on('viviendas')->onDelete('cascade');
            
            $table->integer('user')->nullable()->default(null)->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');

            $table->integer('state')->nullable()->default(1);
            $table->integer('tipo')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_visita');
    }
}
