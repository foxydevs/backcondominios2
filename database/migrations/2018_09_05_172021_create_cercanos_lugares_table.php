<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCercanosLugaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cercanos_lugares', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion')->nullable()->default(null);
            $table->string('nombre')->nullable()->default(null);

            $table->date('fecha')->nullable()->default(null);
            $table->string('ubicacion')->nullable()->default(null);
            $table->string('direccion')->nullable()->default(null);
            $table->double('cantidad')->nullable()->default(null);
            $table->double('latitud',15,7)->nullable()->default(null);
            $table->double('logitud',15,7)->nullable()->default(null);

            $table->integer('cercano')->nullable()->default(null)->unsigned();
            $table->foreign('cercano')->references('id')->on('cercanos')->onDelete('cascade');

            $table->integer('condominio')->nullable()->default(null)->unsigned();
            $table->foreign('condominio')->references('id')->on('condominios')->onDelete('cascade');
            
            $table->integer('state')->nullable()->default(1);
            $table->integer('tipo')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cercanos_lugares');
    }
}
