<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotacionUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votacion_usuario', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('votacion')->nullable()->default(null)->unsigned();
            $table->foreign('votacion')->references('id')->on('votacion')->onDelete('cascade');
            $table->integer('user')->nullable()->default(null)->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            $table->integer('tipo_res')->nullable()->default(null)->unsigned();
            $table->foreign('tipo_res')->references('id')->on('tipo_respuesta')->onDelete('cascade');
            
            $table->integer('state')->nullable()->default(1);
            $table->integer('tipo')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votacion_usuario');
    }
}
