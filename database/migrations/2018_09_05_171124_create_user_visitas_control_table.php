<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserVisitasControlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_visitas_control', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha')->nullable()->default(null);
            $table->time('hora')->nullable()->default(null);

            $table->timestamp('aut_fecha')->useCurrent();

            $table->integer('visita')->nullable()->default(null)->unsigned();
            $table->foreign('visita')->references('id')->on('user_visita')->onDelete('cascade');
            
            $table->integer('state')->nullable()->default(1);
            $table->integer('tipo')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_visitas_control');
    }
}
