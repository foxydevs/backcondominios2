<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCobrosUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cobros_usuario', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('cobro')->nullable()->default(null)->unsigned();
            $table->foreign('cobro')->references('id')->on('cobros')->onDelete('cascade');
            $table->integer('vivienda')->nullable()->default(null)->unsigned();
            $table->foreign('vivienda')->references('id')->on('viviendas')->onDelete('cascade');
            $table->timestamp('fecha')->useCurrent();
            $table->timestamp('fecha_pagado')->nullable()->default(null);

            $table->integer('pagado')->nullable()->default(1);
            $table->integer('state')->nullable()->default(1);
            $table->integer('tipo')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cobros_usuario');
    }
}
