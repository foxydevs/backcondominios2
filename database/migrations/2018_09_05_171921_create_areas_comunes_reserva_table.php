<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasComunesReservaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas_comunes_reserva', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_inicio')->nullable()->default(null);
            $table->date('fecha_fin')->nullable()->default(null);
            $table->date('hora_inicio')->nullable()->default(null);
            $table->date('hora_fin')->nullable()->default(null);
            $table->string('comentarios')->nullable()->default(null);
            $table->double('costo')->nullable()->default(null);

            $table->integer('user')->nullable()->default(null)->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            $table->integer('area_comun')->nullable()->default(null)->unsigned();
            $table->foreign('area_comun')->references('id')->on('areas_comunes')->onDelete('cascade');
            
            $table->integer('state')->nullable()->default(1);
            $table->integer('tipo')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas_comunes_reserva');
    }
}
