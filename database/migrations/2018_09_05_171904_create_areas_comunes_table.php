<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasComunesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas_comunes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion')->nullable()->default(null);
            $table->string('nombre')->nullable()->default(null);
            $table->string('foto')->nullable()->default(null);
            $table->string('foto1')->nullable()->default(null);
            $table->string('foto2')->nullable()->default(null);
            $table->string('foto3')->nullable()->default(null);

            $table->double('costo')->nullable()->default(null);

            $table->integer('state')->nullable()->default(1);
            $table->integer('tipo')->nullable()->default(1);

            $table->integer('condominio')->nullable()->default(null)->unsigned();
            $table->foreign('condominio')->references('id')->on('condominios')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas_comunes');
    }
}
