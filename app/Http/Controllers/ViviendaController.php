<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Viviendas;
use App\ViviendaUsuarios;
use Response;
use Validator;

class ViviendaController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(Viviendas::all(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = Viviendas::whereRaw('state=?',[$id,$state])->get();
                    break;
                }
                case 'type':{
                    $objectSee = Viviendas::whereRaw('tipo=?',[$id,$state])->get();
                    break;
                }
                default:{
                    $objectSee = Viviendas::whereRaw('state=?',[$id,$state])->get();
                    break;
                }
    
            }
        }else{
            $objectSee = Viviendas::whereRaw('state=?',[$id,$state])->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByUser($id)
    {
        $objectSee = ViviendaUsuarios::select('user')->where('vivienda','=',$id)->get();
        if ($objectSee) {

            $objectSee1 = Viviendas::whereIn('id',$objectSee)->get();

            
    
            return Response::json($objectSee1, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByClient($id)
    {
        $objectSee = Viviendas::where('app','=',$id)->with('users')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'numero'          => 'required',
            'direccion'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Viviendas();
                $newObject->ubicacion       = $request->get('ubicacion');
                $newObject->numero          = $request->get('numero');
                $newObject->direccion       = $request->get('direccion');
                $newObject->comentarios     = $request->get('comentarios');
                $newObject->longitud        = $request->get('longitud');
                $newObject->latitud         = $request->get('latitud');
                $newObject->state           = $request->get('state');
                $newObject->tipo            = $request->get('tipo');
                $newObject->condominio      = $request->get('condominio');
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    public function uploadAvatar(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'avatar'      => 'required|image|mimes:jpeg,png,jpg'
        ]);
    
        if ($validator->fails()) {
            $returnData = array(
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator->messages()->toJson()
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
    
                $path = Storage::disk('s3')->put($request->carpeta, $request->avatar);
    
                $objectUpdate->picture = Storage::disk('s3')->url($path);
                $objectUpdate->save();
    
                return Response::json($objectUpdate, 200);
    
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
            }
    
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = Viviendas::find($id);
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = Viviendas::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->ubicacion       = $request->get('ubicacion', $objectUpdate->ubicacion);
                $objectUpdate->numero          = $request->get('numero', $objectUpdate->numero);
                $objectUpdate->direccion       = $request->get('direccion', $objectUpdate->direccion);
                $objectUpdate->comentarios     = $request->get('comentarios', $objectUpdate->comentarios);
                $objectUpdate->longitud        = $request->get('longitud', $objectUpdate->longitud);
                $objectUpdate->latitud         = $request->get('latitud', $objectUpdate->latitud);
                $objectUpdate->state           = $request->get('state', $objectUpdate->state);
                $objectUpdate->tipo            = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->condominio            = $request->get('condominio', $objectUpdate->condominio);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = Viviendas::find($id);
        if ($objectDelete) {
            try {
                Viviendas::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
