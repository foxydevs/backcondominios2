<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\ProyectoPropuestas;
use Response;
use Validator;

class ProyectoPropuestasController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(ProyectoPropuestas::all(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = ProyectoPropuestas::whereRaw('user=? and state=?',[$id,$state])->with('user')->get();
                    break;
                }
                case 'type':{
                    $objectSee = ProyectoPropuestas::whereRaw('user=? and tipo=?',[$id,$state])->with('user')->get();
                    break;
                }
                default:{
                    $objectSee = ProyectoPropuestas::whereRaw('user=? and state=?',[$id,$state])->with('user')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = ProyectoPropuestas::whereRaw('user=? and state=?',[$id,$state])->with('user')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByUser($id)
    {
        $objectSee = ProyectoPropuestas::where('app','=',$id)->with('users')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByClient($id)
    {
        $objectSee = ProyectoPropuestas::where('app','=',$id)->with('users')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'proyecto'          => 'required',
            'user'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new ProyectoPropuestas();
                $newObject->descripcion       = $request->get('descripcion');
                $newObject->titulo            = $request->get('titulo');
                $newObject->fecha             = $request->get('fecha');
                $newObject->comentarios       = $request->get('comentarios');
                $newObject->foto              = $request->get('foto');
                $newObject->cantidad          = $request->get('cantidad');
                $newObject->costo             = $request->get('costo');
                $newObject->proyecto          = $request->get('proyecto');
                $newObject->user              = $request->get('user');
                $newObject->state             = $request->get('state');
                $newObject->tipo              = $request->get('tipo');
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    public function uploadAvatar(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'avatar'      => 'required|image|mimes:jpeg,png,jpg'
        ]);
    
        if ($validator->fails()) {
            $returnData = array(
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator->messages()->toJson()
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
    
                $path = Storage::disk('s3')->put($request->carpeta, $request->avatar);
    
                $objectUpdate->picture = Storage::disk('s3')->url($path);
                $objectUpdate->save();
    
                return Response::json($objectUpdate, 200);
    
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
            }
    
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = ProyectoPropuestas::find($id);
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = ProyectoPropuestas::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->descripcion       = $request->get('descripcion', $objectUpdate->descripcion);
                $objectUpdate->titulo            = $request->get('titulo', $objectUpdate->titulo);
                $objectUpdate->fecha             = $request->get('fecha', $objectUpdate->fecha);
                $objectUpdate->comentarios       = $request->get('comentarios', $objectUpdate->comentarios);
                $objectUpdate->foto              = $request->get('foto', $objectUpdate->foto);
                $objectUpdate->cantidad          = $request->get('cantidad', $objectUpdate->cantidad);
                $objectUpdate->costo             = $request->get('costo', $objectUpdate->costo);
                $objectUpdate->proyecto          = $request->get('proyecto', $objectUpdate->proyecto);
                $objectUpdate->user              = $request->get('user', $objectUpdate->user);
                $objectUpdate->state             = $request->get('state', $objectUpdate->state);
                $objectUpdate->tipo              = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = ProyectoPropuestas::find($id);
        if ($objectDelete) {
            try {
                ProyectoPropuestas::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
