<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;

use App\Http\Requests;
use App\Users;
use App\CobrosUsuario;
use App\Cobros;
use App\Viviendas;
use Faker\Factory as Faker;
use Response;
use Validator;
use Hash;
use Storage;
use DB;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Users::all(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = Users::whereRaw('user=? and state=?',[$id,$state])->with('user')->get();
                    break;
                }
                case 'type':{
                    $objectSee = Users::whereRaw('user=? and tipo=?',[$id,$state])->with('user')->get();
                    break;
                }
                default:{
                    $objectSee = Users::whereRaw('user=? and state=?',[$id,$state])->with('user')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = Users::whereRaw('user=? and state=?',[$id,$state])->with('user')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByUser($id)
    {
        $objectSee = Users::where('app','=',$id)->with('users')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getMorosos()
    {
        $nowD = date('Y-m-d');
        $nowT = date('H:i:s');
        // $nowD = "2018-09-25";
        // $nowT = "21:08:21";
        $cobros = Cobros::select('id')->whereRaw('(fecha<?)',[$nowD])->get();
        
        $objectSee = CobrosUsuario::select('vivienda')->whereIn('cobro',$cobros)->whereRaw('pagado=0')->get();
        if ($objectSee) {
            
            $objectSeeF = Viviendas::whereIn('id',$objectSee)->get();
            
            return Response::json($objectSeeF, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getDirectivos()
    {
        $objectSee = Users::whereRaw('directivo=1')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByClient($id)
    {
        $objectSee = Users::where('app','=',$id)->with('users')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *'password'      => 'required|min:3|regex:/^.*(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!-,:-@]).*$/',
     */
     public function store(Request $request)
     {
         $validator = Validator::make($request->all(), [
             'username'      => 'required',
             'password'      => 'required|min:3',
             'email'         => 'required|email'
         ]);
         
 
         if ($validator->fails()) {
             $returnData = array(
                 'status' => 400,
                 'message' => 'Invalid Parameters',
                 'validator' => $validator->messages()->toJson()
             );
             return Response::json($returnData, 400);
         }
         else {
             $email = $request->get('email');
             $email_exists  = Users::whereRaw("email = ?", $email)->count();
             $user = $request->get('username');
             $user_exists  = Users::whereRaw("username = ?", $user)->count();
             if($email_exists == 0 && $user_exists == 0){    
                 try {
                     $newObject = new Users();
                     $newObject->username = $request->get('username');
                     $newObject->password = Hash::make($request->get('password'));
                     $newObject->email = $email;
                     $newObject->picture = $request->get('picture',"http://foxylabs.xyz/Documentos/imgs/logo.png");
                     $newObject->tipo = $request->get('tipo');
                     $newObject->state = $request->get('state',21);
                     $newObject->nombre = $request->get('nombre');
                     $newObject->apellido = $request->get('apellido');
                     $newObject->dpi = $request->get('dpi');
                     $newObject->descripcion = $request->get('descripcion');
                     $newObject->nacimiento = $request->get('nacimiento');
                     $newObject->telefono = $request->get('telefono');
                     $newObject->youtube_channel = $request->get('youtube_channel');
                     $newObject->directivo = $request->get('directivo',0);
                     $newObject->casa = $request->get('casa');
                     $newObject->puesto = $request->get('puesto');
                     $newObject->last_conection = date('Y-m-d H:i:s');
                     $newObject->save();
                     
                     Mail::send('emails.confirm', ['empresa' => 'GTechnology', 'url' => 'http://gtechnology.gt', 'app' => 'http://me.gtechnology.gt', 'password' => $request->get('password'), 'username' => $newObject->username, 'email' => $newObject->email, 'name' => $newObject->nombre.' '.$newObject->apellido,], function (Message $message) use ($newObject){
                        $message->from('info@foxylabs.gt', 'Info GTechnology')
                                ->sender('info@foxylabs.gt', 'Info GTechnology')
                                ->to($newObject->email, $newObject->nombre.' '.$newObject->apellido)
                                ->replyTo('info@foxylabs.gt', 'Info GTechnology')
                                ->subject('Usuario Creado');
                    
                    });
                     return Response::json($newObject, 200);
                 }
                 catch(Exception $e) {
                     $returnData = array(
                         'status' => 500,
                         'message' => $e->getMessage()
                     );
                     return Response::json($returnData, 500);
                 }
             } else {
                $returnData = array(
                     'status' => 400,
                     'message' => 'User already exists',
                     'validator' => $validator->messages()->toJson()
                 );
                 return Response::json($returnData, 400);
             }
         }
     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Users::find($id);
        if ($objectSee) {

            return Response::json($objectSee, 200);

        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Users::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->username = $request->get('username', $objectUpdate->username);
                $objectUpdate->email = $request->get('email', $objectUpdate->email);
                $objectUpdate->picture = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->tipo = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->state = $request->get('state', $objectUpdate->state);
                $objectUpdate->nombre = $request->get('nombre', $objectUpdate->nombre);
                $objectUpdate->apellido = $request->get('apellido', $objectUpdate->apellido);
                $objectUpdate->dpi = $request->get('dpi', $objectUpdate->dpi);
                $objectUpdate->descripcion = $request->get('descripcion', $objectUpdate->descripcion);
                $objectUpdate->nacimiento = $request->get('nacimiento', $objectUpdate->nacimiento);
                $objectUpdate->telefono = $request->get('telefono', $objectUpdate->telefono);
                $objectUpdate->youtube_channel = $request->get('youtube_channel', $objectUpdate->youtube_channel);
                $objectUpdate->casa = $request->get('casa', $objectUpdate->casa);
                $objectUpdate->puesto = $request->get('puesto', $objectUpdate->puesto);
                $objectUpdate->directivo = $request->get('directivo', $objectUpdate->directivo);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            }catch (\Illuminate\Database\QueryException $e) {
                if($e->errorInfo[0] == '01000'){
                    $errorMessage = "Error Constraint";
                }  else {
                    $errorMessage = $e->getMessage();
                }
                $returnData = array (
                    'status' => 505,
                    'SQLState' => $e->errorInfo[0],
                    'message' => $errorMessage
                );
                return Response::json($returnData, 500);
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
            }
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function uploadAvatar(Request $request, $id) {
        $objectUpdate = Users::find($id);
        if ($objectUpdate) {

            $validator = Validator::make($request->all(), [
                'avatar'      => 'required|image|mimes:jpeg,png,jpg'
            ]);

            if ($validator->fails()) {
                $returnData = array(
                    'status' => 400,
                    'message' => 'Invalid Parameters',
                    'validator' => $validator->messages()->toJson()
                );
                return Response::json($returnData, 400);
            }
            else {
                try {
                    $path = Storage::disk('s3')->put('avatar', $request->avatar);

                    $objectUpdate->picture = Storage::disk('s3')->url($path);
                    $objectUpdate->save();

                    return Response::json($objectUpdate, 200);
                }
                catch (Exception $e) {
                    $returnData = array(
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                }

            }

            return Response::json($objectUpdate, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function recoveryPassword(Request $request){
        $objectUpdate = Users::whereRaw('email=? or username=?',[$request->get('username'),$request->get('username')])->first();
        if ($objectUpdate) {
            try {
                $faker = Faker::create();
                $pass = $faker->password();
                $objectUpdate->password = bcrypt($pass);
                $objectUpdate->state = 21;
                
                Mail::send('emails.recovery', ['empresa' => 'GTechnology', 'url' => 'http://gtechnology.gt', 'password' => $pass, 'email' => $objectUpdate->email, 'name' => $objectUpdate->nombre.' '.$objectUpdate->apellido,], function (Message $message) use ($objectUpdate){
                    $message->from('info@foxylabs.gt', 'Info GTechnology')
                            ->sender('info@foxylabs.gt', 'Info GTechnology')
                            ->to($objectUpdate->email, $objectUpdate->nombre.' '.$objectUpdate->apellido)
                            ->replyTo('info@foxylabs.gt', 'Info GTechnology')
                            ->subject('Contraseña Reestablecida');
                
                });
                
                $objectUpdate->save();
                
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function changePassword(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'new_pass' => 'required|min:3',
            'old_pass'      => 'required'
        ]);

        if ($validator->fails()) {
            $returnData = array(
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator->messages()->toJson()
            );
            return Response::json($returnData, 400);
        }
        else {
            $old_pass = $request->get('old_pass');
            $new_pass_rep = $request->get('new_pass_rep');
            $new_pass =$request->get('new_pass');
            $objectUpdate = Users::find($id);
            if ($objectUpdate) {
                try {
                    if(Hash::check($old_pass, $objectUpdate->password))
                    {                       
                        if($new_pass_rep != $new_pass)
                        {
                            $returnData = array(
                                'status' => 404,
                                'message' => 'Passwords do not match'
                            );
                            return Response::json($returnData, 404);
                        }

                        if($old_pass == $new_pass)
                        {
                            $returnData = array(
                                'status' => 404,
                                'message' => 'New passwords it is same the old password'
                            );
                            return Response::json($returnData, 404);
                        }
                        $objectUpdate->password = Hash::make($new_pass);
                        $objectUpdate->state = 1;
                        $objectUpdate->save();

                        return Response::json($objectUpdate, 200);
                    }else{
                        $returnData = array(
                            'status' => 404,
                            'message' => 'Invalid Password'
                        );
                        return Response::json($returnData, 404);
                    }
                }
                catch (Exception $e) {
                    $returnData = array(
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                }
            }
            else {
                $returnData = array(
                    'status' => 404,
                    'message' => 'No record found'
                );
                return Response::json($returnData, 404);
            }
        }
    }
    public function deleteAvatar($id)
    {
        try{
            $objectUpdate = Users::find($id);
            if ($objectUpdate) {
                try {
                    $direcciones = explode("/",str_replace('https://','',$objectUpdate->picture));
                    $name = $direcciones[3];
                    $path = Storage::disk('s3')->delete('avatar/'.$name);
                    $objectUpdate->picture = '';
                    $objectUpdate->save();

                    return Response::json([$objectUpdate, $name], 200);
                }
                catch (Exception $e) {
                    $returnData = array(
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                }
            }

            return Response::json($objectUpdate, 200);
        }catch(Exception $e){
            $returnData = array (
                'status' => 501,
                'message' => $e->getMessage()
            );
            return Response::json($returnData, 501);
        }
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Users::find($id);
        if ($objectDelete) {
            try {
                Users::destroy($id);
                return Response::json($objectDelete, 200);
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
