<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPuesto extends Model
{
    protected $table = 'tipo_puesto';
}
