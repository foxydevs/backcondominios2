<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ObjetosPerdidos extends Model
{
    protected $table = 'objetos_perdidos';
}
