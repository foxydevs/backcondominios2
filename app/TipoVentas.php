<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoVentas extends Model
{
    protected $table = 'tipo_ventas';
}
