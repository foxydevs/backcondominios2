<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoRespuesta extends Model
{
    protected $table = 'tipo_respuesta';
}
