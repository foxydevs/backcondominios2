<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreasComunesReserva extends Model
{
    protected $table = 'areas_comunes_reserva';
}
